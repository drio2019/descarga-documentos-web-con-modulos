#!/usr/bin/python3

from robot import Robot
from cache import Cache


def main():

    print("Robot class")
    r1 = Robot('http://gsyc.urjc.es/')
    r1.show()
    r1.retrieve()
    r1.retrieve()

    r2 = Robot('https://www.aulavirtual.urjc.es')
    r2.show()

    print("\nCache class")
    c = Cache()
    c.retrieve('http://gsyc.urjc.es/')
    c.show('http://gsyc.urjc.es/')
    c.show_all()

    c.retrieve('https://www.aulavirtual.urjc.es')
    c.show('https://www.aulavirtual.urjc.es')
    c.show_all()


if __name__ == '__main__':
    main()
