#!/usr/bin/python3

import urllib.request


class Robot:

    def __init__(self, url):
        self.url = url
        self.retrieved = False
        print(self.url)

    def retrieve(self):
        if not self.retrieved:
            print(f"Downloading... {self.url}")
            data = urllib.request.urlopen(self.url)
            self.content = data.read().decode('utf-8')
            self.retrieved = True

    def content(self):
        self.retrieve()
        return self.content

    def show(self):
        print(self.content())


